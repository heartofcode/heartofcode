
# [Heart of Code](https://heartofcode.net)
https://heartofcode.net

JAMstack website built on GatsbyJS and styled-components. Tests run with Jest and React-Testing-Library.
 
**GatsbyJS** was chosen because it is built on React, bringing with it the entire JavaScript ecosystem, and unified data consumption through GraphQL.

**Styled-Components** was chosen because it solves several issues of traditional CSS without the typical shortcomings of CSS-in-JS solutions.

- Class names are uniquely generated to guarantee there is no name clashing.
- Styling lives alongside React components, making it easier to understand how the styling is used.
- Psuedo-selectors, media queries, and everything else that CSS can do is supported.
- Dynamic styling with props and themes.

**React-Testing-Library** was chosen because of its emphasis on testing user behaviours instead of implementation details. Building tests that resemble how a user is going to interact with the software encourages us to focus the tests on the expected input and output, which should not change often, rather than implementation details.