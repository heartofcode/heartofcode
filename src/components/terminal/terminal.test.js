
import React from 'react'
import { fireEvent, render, waitForElement } from '@testing-library/react'
import '@testing-library/jest-dom'

import { Terminal } from './terminal'

describe('Terminal', () => {

    it('renders one initial message', async () => {

        const {getByText} = render(
            <Terminal delay={0}>
                {'One'}
            </Terminal>
        )

        let inputOne = await waitForElement(() => getByText('One'))

        expect(inputOne).toBeInTheDocument()

    })

    it('responds to user input', async () => {

        const {getByTestId, getAllByTestId} = render(
            <Terminal delay={0}>
                {'One'}
                {'Two'}
            </Terminal>
        )

        // track the # of lines and responses that we expect in the terminal
        let expectLines = 2 
        let expectResponses = 0

        // wait for the input field to render
        const queryField = await waitForElement(() => getByTestId('terminal-query'))

        // data for the input field
        let userInput = 'what is your name'

        fireEvent.change(queryField, { target: { value: userInput } })

        // validate the input field
        expect(queryField.value).toBe(userInput)

        // submit input
        fireEvent.keyDown(queryField, { key: 'Enter', code: 13, charCode: 13 })
        expectLines += 1

        // wait for a response
        await waitForElement(() => getByTestId('terminal-response'))
        expectResponses += 1

        const terminalLines = getAllByTestId('terminal-line')
        const terminalResponse = getAllByTestId('terminal-response')

        // check our total # of lines and responses
        expect(terminalLines.length).toBe(expectLines)
        expect(terminalResponse.length).toBe(expectResponses)

    })

})